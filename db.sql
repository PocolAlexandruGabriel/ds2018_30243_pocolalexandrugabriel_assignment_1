CREATE DATABASE  IF NOT EXISTS `assignment-two`;
USE `assignment-two`;

DROP TABLE IF EXISTS `flight`;
CREATE TABLE `flight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flightNumber` int(11) NOT NULL,
  `airplaneType` varchar(45) NOT NULL,
  `departureCity` varchar(45) NOT NULL,
  `departureDateTime` date NOT NULL,
  `arrivalCity` varchar(45) NOT NULL,
  `arrivalDateTime` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `permission` int(11) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB;
