<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/style/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Add flight</title>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">Flights Management</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/home">Home</a></li>
            <li><a href="/flights">Flights</a></li>
            <li><a href="/add">Add Flight</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <form action="/edit" method="post">
        <div class="form-group">
            <label for="flightNumber">Flight id:</label>
            <input type="number" value=${flight.id} class="form-control" id="flightId" name="id" readonly>
        </div>
        <div class="form-group">
            <label for="flightNumber">Flight number:</label>
            <input type="number" value=${flight.flightNumber} class="form-control" id="flightNumber" name="flightNumber" required>
        </div>
        <div class="form-group">
            <label for="airplaneType">Airplane type:</label>
            <input type="text" value=${flight.airplaneType} class="form-control" id="airplaneType" name="airplaneType" required>
        </div>
        <div class="form-group">
            <label for="departureCity">Departure city:</label>
            <select class="form-control" id="departureCity" name="departureCity" required>
                <c:forEach var="city" items="${citiesList}">
                    <option value=${city.name}>${city.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="departureDateTime">Departure date & time:</label>
            <input type="datetime-local" value="${flight.departureDateTime}" class="form-control" id="departureDateTime" name="departureDateTime" required>
        </div>
        <div class="form-group">
            <label for="arrivalCity">Arrival city:</label>
            <select class="form-control" id="arrivalCity" name="arrivalCity" required>
                <c:forEach var="city" items="${citiesList}">
                    <option value=${city.name}>${city.name}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="arrivalDateTime">Arrival date & time:</label>
            <input type="datetime-local" value=${flight.arrivalDateTime} class="form-control" id="arrivalDateTime" name="arrivalDateTime" required>
        </div>
        <button type="submit" class="btn btn-default">Edit flight</button>
    </form>
</div>
</body>
</html>

