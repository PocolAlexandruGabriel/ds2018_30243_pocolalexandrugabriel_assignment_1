<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/style/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Flights</title>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">Flights Management</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/home">Home</a></li>
            <li class="active"><a href="/flights">Flights</a></li>
            <c:if test="${permission == 1}">
                <li><a href="/add">Add Flight</a></li>
            </c:if>>
        </ul>
    </div>
</nav>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Flight number</th>
            <th>Airplane type</th>
            <th>Departure city</th>
            <th>Departure date & time</th>
            <th>Arrival city</th>
            <th>Arrival date & time</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="flight" items="${flightsList}">
            <tr>
                <td>${flight.flightNumber}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.departureCity}</td>
                <td>${flight.departureDateTime}</td>
                <td>${flight.arrivalCity}</td>
                <td>${flight.arrivalDateTime}</td>
                <c:if test="${permission == 1}">
                    <td>
                        <form class="actions">
                            <div class="btn-group btn-group-xs">
                                <button type="submit" name="delete" value="${flight.id}" class="btn btn-primary"
                                        formmethod="post"
                                        formaction="/delete">Delete
                                </button>
                                <button type="submit" name="edit" value="${flight.id}" class="btn btn-primary"
                                        formmethod="get"
                                        formaction="/edit">Edit
                                </button>
                            </div>
                        </form>
                    </td>
                </c:if>
                <c:if test="${permission == 0}">
                    <td>
                        <form class="actions">
                            <div class="btn-group btn-group-xs">
                                <button type="submit" name="local" value="${flight.arrivalCity}" class="btn btn-primary"
                                        formmethod="post"
                                        formaction="/local">Local time
                                </button>
                            </div>
                        </form>
                    </td>
                </c:if>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <p>Local time: ${local}</p>
</div>
</body>
</html>
