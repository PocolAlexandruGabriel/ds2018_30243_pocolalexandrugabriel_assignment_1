<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/style/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Home</title>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">Flights Management</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/home">Home</a></li>
            <li><a href="/flights">Flights</a></li>
            <c:if test="${permission == 1}">
                <li><a href="/add">Add Flight</a></li>
            </c:if>
        </ul>
    </div>
</nav>
<marquee behavior="scroll" direction="right">
    <img src="https://www.freeiconspng.com/uploads/airplane-png-33.png" width="350" alt="Airplane Clipart Png Best"
         class="airplane-image"/>
</marquee>
</body>
</html>