package daos;

import models.Flight;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import sun.plugin2.os.windows.FLASHWINFO;

import java.util.List;

public class FlightDAO {

    private SessionFactory sessionFactory;

    public FlightDAO() {
       configure();
    }


    public void configure() {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry =new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public Flight addFlight(Flight flight) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlight(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<Flight> getFlights() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight");
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights;
    }

    @SuppressWarnings("unchecked")
    public Flight deleteFlight(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("DELETE Flight WHERE id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return findFlight(id);
    }

    @SuppressWarnings("unchecked")
    public void updateFlight(Flight flight) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("UPDATE Flight SET flightNumber = :flightNumber, " +
                    "airplaneType = :airplaneType, departureCity = :departureCity, " +
                    "departureDateTime = :departureDateTime, arrivalCity = :arrivalCity, " +
                    "arrivalDateTime = :arrivalDateTime WHERE id = :id");
            query.setParameter("id", flight.getId());
            query.setParameter("flightNumber",flight.getFlightNumber());
            query.setParameter("airplaneType", flight.getAirplaneType());
            query.setParameter("departureCity", flight.getDepartureCity());
            query.setParameter("departureDateTime", flight.getDepartureDateTime());
            query.setParameter( "arrivalCity", flight.getArrivalCity());
            query.setParameter("arrivalDateTime", flight.getArrivalDateTime());
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
    }
}

