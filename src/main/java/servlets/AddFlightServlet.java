package servlets;

import daos.CityDAO;
import daos.FlightDAO;
import models.City;
import models.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet("/add")
public class AddFlightServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FlightDAO flightDAO = new FlightDAO();
        Flight flight = new Flight();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss");

        flight.setFlightNumber(Integer.parseInt(request.getParameter(("flightNumber"))));
        flight.setAirplaneType(request.getParameter("airplaneType"));
        flight.setDepartureCity(request.getParameter("departureCity"));
        flight.setArrivalCity(request.getParameter("arrivalCity"));

        LocalDateTime departureTime = LocalDateTime.parse(request.getParameter("departureDateTime"));
        flight.setDepartureDateTime(Timestamp.valueOf(departureTime.format(dateTimeFormatter)));

        LocalDateTime arrivalTime = LocalDateTime.parse(request.getParameter("arrivalDateTime"));
        flight.setArrivalDateTime(Timestamp.valueOf(arrivalTime.format(dateTimeFormatter)));

        flightDAO.addFlight(flight);
        response.sendRedirect("/flights");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CityDAO cityDAO = new CityDAO();
        List<City> citiesList = cityDAO.getCities();
        request.setAttribute("citiesList", citiesList);

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/views/addFlight.jsp");
        requestDispatcher.forward(request, response);
    }
}
