package servlets;

import daos.CityDAO;
import models.City;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

@WebServlet(urlPatterns = "/local")
public class LocalTimeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CityDAO cityDAO = new CityDAO();
        City city = cityDAO.findCity(request.getParameter("local"));
        int latitude = city.getLatitude();
        int longitude = city.getLongitude();
        URL url = new URL("http://www.new.earthtools.org/timezone/" + latitude + "/" + longitude);

        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(url);
            String localtime = doc.getRootElement().getChild("localtime").getText();

            request.getSession().setAttribute("local", localtime);
            response.sendRedirect("/flights");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
