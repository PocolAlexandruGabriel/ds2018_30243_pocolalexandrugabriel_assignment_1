package servlets;

import daos.FlightDAO;
import models.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/flights")
public class FlightsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FlightDAO flightDAO = new FlightDAO();
        List<Flight> flightsList = flightDAO.getFlights();
        request.setAttribute("flightsList", flightsList);

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/views/flights.jsp");
        requestDispatcher.forward(request, response);
    }
}
